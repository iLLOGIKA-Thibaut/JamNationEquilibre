﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardGenerator : MonoBehaviour
{

    public InputField InputAtk;
    public InputField InputDef;
    public InputField InputFire;
    public InputField InputSpeed;

    public InputField InputSpellWidth;

    public Transform cheatMenu;

    public Transform hand;
    //public SummonCard summonPrefab;
    //public SpellCard spellPrefab;
    public Card cardPrefab;
    public static CardGenerator instance;

    [System.Serializable]
    public class cardData
    {
        public Sprite Image;
        public int AtkMod;
        public int DefMod;
        public int SpwnSpdMod;
        public bool isSummon;
        public bool FootMan;
        public bool Tank;
        public bool Horse;
        public bool Bowman;


    }


    public List<cardData> normalCards;
    public List<cardData> UncommonCards;
    public List<cardData> RareCards;

    public void GenerateSpellBtn()
    {
        /*
        Card SC = Card.Instantiate(cardPrefab, hand);
        SC.BonusDef = int.Parse(InputDef.text);
        SC.BonusAtk = int.Parse(InputAtk.text);
        SC.BonusSpeed = int.Parse(InputSpeed.text);
        SC.Width = int.Parse(InputSpellWidth.text);
        */

        Card SC = Generate();

        SC.BonusDef = int.Parse(InputDef.text);
        SC.BonusAtk = int.Parse(InputAtk.text);
        SC.BonusSpeed = int.Parse(InputSpeed.text);
        // GenerateSpell();
    }

    public void drawCard()
    {
        Card SC = Generate();
    }

    public Card drawLeaderCard()
    {
        Card SC = Generate(false);
        return SC;
    }

    Card Generate(bool playerCard = true)
    {
        Card SC;
        if (playerCard)
        {
            SC = Card.Instantiate(cardPrefab, hand);
        }
        else
        {
            SC = Card.Instantiate(cardPrefab);
        }
        


        int Rarity = Random.Range(0, 8);

        if (Rarity == 7)
        {
            SC.Rarity = Card.CardRarity.Rare;
            SC.laneSlots = generateSlotsValue(1);
            
            SC.initWithData(RareCards[Random.Range(0, RareCards.Count)]);
        }
        else if (Rarity >= 5)
        {
            SC.Rarity = Card.CardRarity.Unusual;
            SC.laneSlots = generateSlotsValue(3);
            SC.initWithData(UncommonCards[Random.Range(0, UncommonCards.Count)]);
        }
        else
        {
            SC.Rarity = Card.CardRarity.Common;
            SC.laneSlots = generateSlotsValue(4);
            SC.initWithData(normalCards[Random.Range(0, normalCards.Count)]);
        }


        return SC;
    }

    bool[] generateSlotsValue(int slots)
    {
        bool[] laneSlots = new bool[5];

        // init all to false
        for (int i = 0; i < laneSlots.Length; i++)
        {
            laneSlots[i] = false;
        }


        laneSlots[0] = true;
        // randomly give true or false for each slot
        for (int i = 1; i < slots; i++)
        {

            laneSlots[i] = (Random.Range(0, 2) == 0) ? true : false;
        }


        return laneSlots;
        /*
        // from last to first, remove while there is only falses slots, to always start and finish with a true
        for (int i = slots; i > 0; i--)
        {
            int rand = Random.Range(0, 2);
            laneSlots[i] = (Random.Range(0, 2) > 0) ? true : false;
        }
        */


    }


    void LootRandomCard()
    {

    }



    // Use this for initialization
    void Start()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("CheatMenu"))
        {

            ToggleCheatMenu();
        }

    }

    private void ToggleCheatMenu()
    {
        if (cheatMenu.gameObject.activeSelf)
        {
            cheatMenu.gameObject.SetActive(true);
        }
        else
        {
            cheatMenu.gameObject.SetActive(false);
        }
        cheatMenu.gameObject.SetActive(!cheatMenu.gameObject.activeSelf);
    }
}
