﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Spawner : MonoBehaviour {

    [Header("Live Bosts")]
    public int SpawnSpeedBoost = 0;
    public int AttackBoost = 0;
    public int LifeBoost = 0;

    public float SpawnSpeedIncreases = 0.25f;
    public float BaseSpawnTime = 1f;

    public bool Started = false;

    public bool isLeft;
    public Unit CurrentUnit;
    public GameObject SpawnPosition;
    public GameObject LootPrefab;

    private float _lastTimeSpawn = 0f;
    private Lane _associatedLane;
    private Spawner _adverseSpanwer;
    private float _currentLootChance = 0.8f;

    private int MaxUnits = 3;
    public List<Unit> ActiveUnits;

    private Material _materialInstance;
    public Renderer TentRenderer;

    public Image AttackIcon;
    public Image AttackFill;
    private float _attackBoostTimer;

    public Image DefenseIcon;
    public Image DefenseFill;
    private float _defenseBoostTimer;

    public Image SpawnIcon;
    public Image SpawnFill;
    private float _spawnBoostTimer;

    public Renderer GlowRenderer;

    private Tween _punchTween;
    private Tween _colorTween;


    public void InitSpawner(Lane lane, Spawner adverseSpawner)
    {
        _associatedLane = lane;
        _adverseSpanwer = adverseSpawner;
        StartSpawning();
        _materialInstance = TentRenderer.material;
        _materialInstance.SetInt("_Highlighted", 0);
        ChangeBoosts(0, 0, 0, false, true);
        GlowRenderer.material = GlowRenderer.material;
    }

    public void StartSpawning()
    {
        Started = true;
        _lastTimeSpawn = Time.time;
    }

    public void SpawnSpecialUnit(Unit unitPrefab)
    {
        SpawnUnit(unitPrefab);
    }

    public void ChangeBoosts(int spawnSpeed, int attack, int life, bool showFX, bool UpdateUI)
    {

        if (!UpdateUI) _spawnBoostTimer = GameManager.instance.BoostTime;
        SpawnSpeedBoost = spawnSpeed;
        
        if(SpawnSpeedBoost == 0)
        {
            SpawnIcon.gameObject.SetActive(false);
            SpawnFill.gameObject.SetActive(false);
        }
        else
        {
            SpawnIcon.gameObject.SetActive(true);
            SpawnFill.gameObject.SetActive(true);
        }

        if (!UpdateUI) _attackBoostTimer = GameManager.instance.BoostTime;
        AttackBoost = attack;
        
        if (AttackBoost == 0)
        {
            AttackIcon.gameObject.SetActive(false);
            AttackFill.gameObject.SetActive(false);
        }
        else
        {
            AttackIcon.gameObject.SetActive(true);
            AttackFill.gameObject.SetActive(true);
        }


        if (!UpdateUI) _defenseBoostTimer = GameManager.instance.BoostTime;
        LifeBoost = life;
        
        if (LifeBoost == 0)
        {
            DefenseIcon.gameObject.SetActive(false);
            DefenseFill.gameObject.SetActive(false);
        }
        else
        {
            DefenseIcon.gameObject.SetActive(true);
            DefenseFill.gameObject.SetActive(true);
        }

        UpdateUnitBoosts();

        if (!showFX) return;

        if (life != 0 || attack != 0 || spawnSpeed != 0)
        {
            AnimateCardUse();
        }

    }

    public void AnimateCardUse()
    {
        if (_punchTween != null) _punchTween.Complete();
        if (_colorTween != null) _colorTween.Complete();
        _colorTween = GlowRenderer.material.DOColor(GameManager.instance.GlowColor, "_TintColor", 0.35f).SetEase(GameManager.instance.BoostGlowAnimation);
        _punchTween = transform.DOPunchScale(new Vector3(-0.01f, 0.005f, -0.01f), 0.4f, 4, 0.5f);
    }

    void UpdateUnitBoosts()
    {
        foreach(Unit u in ActiveUnits)
        {
            u.UpdateBoosts(AttackBoost, LifeBoost, SpawnSpeedBoost);
        }
    }

    public void ShowSelected()
    {
        _materialInstance.SetInt("_Highlighted", 1);
    }

    public void HideSelected()
    {
        _materialInstance.SetInt("_Highlighted", 0);
    }

    // Update is called once per frame
    void Update () {
        if (Started)
        {
            if(Time.time - _lastTimeSpawn >= BaseSpawnTime && ActiveUnits.Count < MaxUnits)
            {
                SpawnUnit(CurrentUnit);
                _lastTimeSpawn = Time.time;
            }

            if(AttackBoost != 0)
            {
                _attackBoostTimer -= Time.deltaTime;
                AttackFill.fillAmount = _attackBoostTimer / GameManager.instance.BoostTime;
                if(_attackBoostTimer <= 0f)
                {
                    ChangeBoosts(SpawnSpeedBoost, 0, LifeBoost, false, true);
                }
            }

            if(LifeBoost != 0)
            {
                _defenseBoostTimer -= Time.deltaTime;
                DefenseFill.fillAmount = _defenseBoostTimer / GameManager.instance.BoostTime;
                if(_defenseBoostTimer <= 0f)
                {
                    ChangeBoosts(SpawnSpeedBoost, AttackBoost, 0, false, true);
                }
            }

            if(SpawnSpeedBoost != 0)
            {
                _spawnBoostTimer -= Time.deltaTime;
                SpawnFill.fillAmount = _spawnBoostTimer / GameManager.instance.BoostTime;
                if(_spawnBoostTimer <= 0f)
                {
                    ChangeBoosts(0, AttackBoost, LifeBoost, false, true);
                }
            }



            if(ActiveUnits.Count != 0)
            {
                for(int x = 0; x < ActiveUnits.Count; x++)
                {
                    if(x == 0)
                    {
                        ActiveUnits[x].UpdateGoalPosition(_adverseSpanwer.ActiveUnits.Count > 0 ? _adverseSpanwer.ActiveUnits[0].transform.position : _adverseSpanwer.transform.position);
                    }
                    else
                    {
                        ActiveUnits[x].UpdateGoalPosition(ActiveUnits[x - 1].transform.position - (transform.forward * 1.3f));
                    }
                }
            }
        }
	}

    public void SendAttack(float damage, int startIndex, int attackLength)
    {
        _adverseSpanwer.ReceiveAttack(damage * (AttackBoost > 0 ? 1.5f : 1f), startIndex, attackLength);
        if (isLeft) GameManager.instance.AttackLeft.PlaySFX();
        else GameManager.instance.AttackRight.PlaySFX();
    }

    public void ReceiveAttack(float damage, int startIndex, int attackLength)
    {
        if(startIndex < ActiveUnits.Count)
        {
            for(int x = startIndex; x < startIndex + attackLength; x++)
            {
                if(x < ActiveUnits.Count)
                {
                    if (ActiveUnits[x].IsHit(damage))
                    {
                        //Die
                        KillUnit(ActiveUnits[x]);
                    }
                }
            }
        }
    }

   public void KillUnit(Unit unit)
    {
        if(unit.LaneIndex +1 < ActiveUnits.Count)
        {
            for (int x = unit.LaneIndex + 1; x < ActiveUnits.Count; x++)
            {
                ActiveUnits[x].LaneIndex--;
            }
        }
        ActiveUnits.Remove(unit);
        unit.KillEffect((transform.forward * -Random.Range(3f, 4f)) + (Vector3.up * 3f), Vector3.zero);
        if(CardGenerator.instance.hand.transform.childCount < 7)
        {
            if (Random.value < GameManager.instance.CurrentLootChance) SpawnLoot(unit.transform.position);
            else GameManager.instance.CurrentLootChance += 0.018f;
        }

        Destroy(unit.gameObject, 2f);
    }

    void SpawnLoot(Vector3 lootPosition)
    {
        GameObject g = Instantiate(LootPrefab, lootPosition, Quaternion.identity);
        g.transform.position = lootPosition;
        g.GetComponent<Loot>().InitLoot();
        GameManager.instance.AddLoot(g.GetComponent<Loot>());
        GameManager.instance.CurrentLootChance *= 0.1f;
    }

    void SpawnUnit(Unit u)
    {

        if (isLeft) GameManager.instance.SpawnSFXLeft.PlaySFX();
        else GameManager.instance.SpawnSFXRight.PlaySFX();

        Unit newUnit = Instantiate(u.gameObject).GetComponent<Unit>();
        ActiveUnits.Add(newUnit);
        
        newUnit.InitUnit(_associatedLane, SpawnPosition.transform, this, ActiveUnits.Count - 1);
        newUnit.UpdateBoosts(AttackBoost, LifeBoost, SpawnSpeedBoost);
    }
}
