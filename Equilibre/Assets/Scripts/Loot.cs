﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Loot : MonoBehaviour {

    public GameObject[] PossibleVisuals;
    public AnimationCurve HeightAnimationCurve;
    Transform animationTarget;
    int test = 0;
    // Use this for initialization
    void Start() {
        animationTarget = GameManager.instance.LootAnimationTarget.transform;

        transform.DOMove(animationTarget.position, 1f).SetEase(Ease.InOutExpo).OnComplete(() => { LootItem(); });
        transform.DORotate(animationTarget.rotation.eulerAngles, 2f,RotateMode.Fast);
    }

    public void InitLoot()
    {
        GameObject visual = PossibleVisuals[Random.Range(0, PossibleVisuals.Length)];
        visual = Instantiate(visual);
        visual.transform.SetParent(this.transform);
        visual.transform.localPosition = Vector3.zero;
        transform.DOMoveZ(transform.position.z - 1f, 1f).SetEase(Ease.OutExpo);
        visual.transform.DOLocalMoveY(0.6f, 1f).SetEase(HeightAnimationCurve);
    }
	
    public void LootItem()
    {
        CardGenerator.instance.drawCard();
        GameManager.instance.LootCardSFX.PlaySFX();
        Destroy(gameObject);
    }

	// Update is called once per frame
	void Update () {
		
	}
}
