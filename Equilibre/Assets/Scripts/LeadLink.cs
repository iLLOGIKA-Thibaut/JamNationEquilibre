﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeadLink : MonoBehaviour {

    public bool IsLeft = false;

    public void SendAction()
    {
        GameManager.instance.PlayLeaderAction(IsLeft);
    }
}
