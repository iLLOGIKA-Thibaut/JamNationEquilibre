﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Card : MonoBehaviour , ISelectHandler{

    public bool Summoner = false;

        public enum Bonuses
    {
        Atk,
        Speed,
        Def,        
    }

    public enum CardRarity
    {
        Common, Unusual, Rare
    }


    public bool affectBothSide = false;
    public Text Title;
    public Text Description;
    public Toggle[] ZonesToggle;
    public Image BackgroundImage;

    public Image AtkImg;
    public Image DefImg;
    public Image SpdImg;
    public Text AtkText;
    public Text DefText;
    public Text SpdText;

    public Image SummonImgArcher;
    public Image SummonImgSoldier;
    public Image SummonImgTank;
    public Image SummonImgHorse;

    public bool SpawnFootman;
    public bool SpawnTank;
    public bool SpawnHorse;
    public bool SpawnBowman;


    public Color modPositif = Color.black;
    public Color modNegatif = Color.red;



    public int Width = 1;

    public CardRarity Rarity;
    public int BonusAtk;
    public int BonusSpeed;
    public int BonusDef;
    public int rarity;

    public bool[] laneSlots= new bool[5];
    public Button button;

    public void refresh()
    {



        // define width
        Width = laneSlots.Length;
        for (int i = laneSlots.Length-1; i > 0; i--)
        {
            if (laneSlots[i]==false)
            {
                Width--;
            }
            else
            {
                break;
            }
        }

        // define slots dots
        for (int i = 0; i < ZonesToggle.Length; i++)
        {
            
            if (i < Width)
            {
                //ZonesToggle[i].isOn = true;
                ZonesToggle[i].gameObject.SetActive(true);
            }
            else
            {

                ZonesToggle[i].gameObject.SetActive(false);
                //ZonesToggle[i].isOn = false;
            }

            ZonesToggle[i].isOn = !laneSlots[i];
        }

        /*    public Image AtkImg;
    public Image DefImg;
    public Image SpdImg;
    public Text AtkText;
    public Text DefText;
    public Text SpdText;*/

        if (BonusAtk != 0)
        {
            AtkImg.gameObject.SetActive(true);
            AtkText.text = BonusAtk.ToString("+0;-#");
            if (BonusAtk < 0) AtkText.color = modNegatif;
        }
        else
        {
            AtkImg.gameObject.SetActive(false);
        }


        if (BonusDef != 0)
        {
            DefImg.gameObject.SetActive(true);
            DefText.text = BonusDef.ToString("+0;-#");
            if (BonusDef < 0) DefText.color = modNegatif;
        }
        else
        {
            DefImg.gameObject.SetActive(false);
        }

        if (BonusSpeed != 0)
        {
            SpdImg.gameObject.SetActive(true);
            SpdText.text = BonusSpeed.ToString("+0;-#");
            if (BonusSpeed < 0) SpdText.color = modNegatif;
        }
        else
        {
            SpdImg.gameObject.SetActive(false);
        }


    }
    // Use this for initialization
    void Start () {


        refresh();

    }
	

	// Update is called once per frame
	void Update () {

        if (EventSystem.current.currentSelectedGameObject == null)
        {
            button.Select();
        }

    }

    public void use(Spawner spawner)
    {
        if (Summoner)
        {
            if (SpawnFootman) GameManager.instance.SpawnUnitFootman(spawner); //
            if (SpawnTank) GameManager.instance.SpawnUnitTank(spawner); //
            if (SpawnHorse) GameManager.instance.SpawnUnitHorse(spawner); //
            if (SpawnBowman) GameManager.instance.SpawnUnitArcher(spawner); //
            spawner.AnimateCardUse();
        }
        else
        {
            spawner.ChangeBoosts(BonusSpeed, BonusAtk, BonusDef, true, false);
        }

        


        GameObject.Destroy(this.gameObject);
    }

    internal void initWithData(CardGenerator.cardData data)
    {
        if (data.isSummon)
        {
            Summoner = true;
            SpawnFootman = data.FootMan;
            SpawnHorse = data.Horse;
            SpawnTank = data.Tank;
            SpawnBowman = data.Bowman;
            Title.text = "Soldat/Soldier";

            if (SpawnBowman) { SummonImgArcher.gameObject.SetActive(true); }
            if (SpawnFootman) { SummonImgSoldier.gameObject.SetActive(true); }
            if (SpawnHorse) { SummonImgHorse.gameObject.SetActive(true); }
            if (SpawnTank) { SummonImgTank.gameObject.SetActive(true); }

        }
        else
        {
            Title.text = "Bonus";
        }
        BackgroundImage.sprite = data.Image;
        BonusAtk = data.AtkMod;
        BonusDef= data.DefMod;
        BonusSpeed = data.SpwnSpdMod;
    }



    public void use(Lane lane)
    {

    }

    public void OnSelect(BaseEventData eventData)
    {
        selectCard();
    }
    public void selectCard()
    {
        GameManager.instance.startSelection(this);
        GameManager.instance.SelectCardSFX.PlaySFX();
        this.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
    }
    public void deselect()
    {
        this.transform.localScale = new Vector3(1f, 1f, 1f);
    }
}
