﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TutoManager : MonoBehaviour {

    public Image image1;
    public Image image2;

    public float minimumTimeToWait;
    float timeLeftTowait;
    private void Start()
    {
        timeLeftTowait = minimumTimeToWait;
    }
    // Update is called once per frame
    void Update () {
        timeLeftTowait -= Time.deltaTime;

        if (timeLeftTowait < 0)
        {
            if (Input.anyKeyDown)
            {
                SwitchImage();

                timeLeftTowait = minimumTimeToWait;
            }      
        }

    }

    private void SwitchImage()
    {
        if (image1.gameObject.activeSelf == true)
        {
            image1.gameObject.SetActive(false);
        }
        else
        {
            SceneManager.LoadScene(2);
        }
    }
}
