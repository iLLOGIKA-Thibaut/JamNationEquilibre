﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayRandomSFX : MonoBehaviour {

    public AudioSource SFXSource;
    public AudioClip[] Clips;
    public bool HasCooldown = false;
    public float cooldownTime = 0.5f;
    private float _lastShot = 0f;

    private void Start()
    {
        SFXSource.volume = PlayerPrefs.GetFloat("SFX", 1f);
    }

    public void PlaySFX()
    {
        if (!HasCooldown)
        {
            SFXSource.clip = Clips[Random.Range(0, Clips.Length)];
            SFXSource.Play();
        }
        else
        {
            if(Time.time - _lastShot > cooldownTime)
            {
                SFXSource.clip = Clips[Random.Range(0, Clips.Length)];
                SFXSource.Play();
                _lastShot = Time.time;
            }
        }

    }
}
