﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public Lane[] ActiveLanes;
    public bool HasEnded = false;

    bool Selecting = false;
    int currentSelectedLane = 0;
    int currentSelectionWidth = 2;
    bool currentPlayerIsLeft;
    public List<Lane> LanesSelecteds = new List<Lane>();
    public List<Spawner> SpawnerSelected = new List<Spawner>();
    Card currentCard;
    public static GameManager instance;
    private List<Loot> _activeLoot;

    public Unit UnitFootmanBlue;
    public Unit UnitTankBlue;
    public Unit UnitHorseBlue;
    public Unit UnitArcherBlue;
    public Unit UnitFootmanRed;
    public Unit UnitTankRed;
    public Unit UnitHorseRed;
    public Unit UnitArcherRed;

    public GameObject LootAnimationTarget;

    private int _score = 0;

    public int Score
    {
        get
        {
            return _score;
        }
        set
        {
            _score = value;
            ScoreText.text = _score.ToString();
            EndScore.text = _score.ToString();
        }
    }

    public Text ScoreText;
    public Text EndScore;

    [Header("Glow Animation")]
    public AnimationCurve BoostGlowAnimation;
    public Color GlowColor;

    public Animator GameOverAnimator;
    public float CurrentLootChance = 0f;

    public float BoostTime = 4f;

    public float TimeBetweenLeaderCards = 30;
    public float RandomLeaderModMultiplier = 0.3f;

    public float LeaderLeftTimer = 20;
    public float LeaderRightTimer = 30;
    public Animation LeftLeaderCardUseAnimation;
    public Animation RightLeaderCardUseAnimation;
    private bool m_isAxisInUse;
    private bool m_isAxisVInUse;

    [Header("Audio")]
    public AudioSource MusicSource;
    public AudioSource AmbianceSource;

    public PlayRandomSFX UseCardSFX;
    public PlayRandomSFX SpawnSFXLeft, SpawnSFXRight;
    public PlayRandomSFX AttackLeft, AttackRight;
    public PlayRandomSFX GameOverSFX;
    public PlayRandomSFX LaneSelectionChangeSFX;
    public PlayRandomSFX LootCardSFX;
    public PlayRandomSFX SelectCardSFX;

    private void Awake()
    {
        instance = this;
    }
    // Use this for initialization
    void Start()
    {
        MusicSource.volume = PlayerPrefs.GetFloat("Music", 1f);
        AmbianceSource.volume = PlayerPrefs.GetFloat("Music", 0.9f) - 0.1f;

        UseCardSFX.SFXSource.volume = PlayerPrefs.GetFloat("SFX", 1f);
        SpawnSFXLeft.SFXSource.volume = PlayerPrefs.GetFloat("SFX", 1f);
        SpawnSFXRight.SFXSource.volume = PlayerPrefs.GetFloat("SFX", 1f);
        AttackLeft.SFXSource.volume = PlayerPrefs.GetFloat("SFX", 1f);
        AttackRight.SFXSource.volume = PlayerPrefs.GetFloat("SFX", 1f);
        GameOverSFX.SFXSource.volume = PlayerPrefs.GetFloat("SFX", 1f);
        LaneSelectionChangeSFX.SFXSource.volume = PlayerPrefs.GetFloat("SFX", 1f);
        LootCardSFX.SFXSource.volume = PlayerPrefs.GetFloat("SFX", 1f);
        SelectCardSFX.SFXSource.volume = PlayerPrefs.GetFloat("SFX", 1f);

        for (int x = 0; x < ActiveLanes.Length; x++)
        {
            ActiveLanes[x].InitLane(x);
        }
        _activeLoot = new List<Loot>();
    }

    public void SpawnUnitFootman(Spawner spawner)
    {
        if (spawner.isLeft)
        {
            spawner.SpawnSpecialUnit(UnitFootmanRed);
        }
        else
        {
            spawner.SpawnSpecialUnit(UnitFootmanBlue);
        }

    }

    public void SpawnUnitTank(Spawner spawner)
    {
        if (spawner.isLeft)
        {
            spawner.SpawnSpecialUnit(UnitTankRed);
        }
        else
        {
            spawner.SpawnSpecialUnit(UnitTankBlue);
        }
    }

    public void SpawnUnitHorse(Spawner spawner)
    {

        if (spawner.isLeft)
        {
            spawner.SpawnSpecialUnit(UnitHorseRed);
        }
        else
        {
            spawner.SpawnSpecialUnit(UnitHorseBlue);

        }

    }

    public void SpawnUnitArcher(Spawner spawner)
    {
        if (spawner.isLeft)
        {
            spawner.SpawnSpecialUnit(UnitArcherRed);
        }
        else
        {
            spawner.SpawnSpecialUnit(UnitArcherBlue);
        }

    }






    public void startSelection(Card card)
    {
        if (currentCard != null)
        {
            currentCard.deselect();
        }

        currentCard = card;
        Selecting = true;
        currentSelectedLane = 0;
        currentSelectionWidth = card.Width;
        SelectLane(0);
    }

    public void AddLoot(Loot loot)
    {
        _activeLoot.Add(loot);
    }

    // Update is called once per frame
    void Update()
    {
        LeaderUpdate();
        bool axeUp = false;
        bool axeDown = false;
        bool axeLeft = false;
        bool axeRight = false;

        if (Selecting)
        {
            if (Input.GetAxisRaw("axePadV") != 0)
            {
                if (m_isAxisVInUse == false)
                {
                    m_isAxisVInUse = true;
                    if (Input.GetAxisRaw("axePadV") > 0.5f)
                    {
                        axeUp = true;
                      // Debug.Log("Up");
                    }

                    if (Input.GetAxisRaw("axePadV") < -0.5f)
                    {
                        axeDown = true;
                        //Debug.Log("down");
                    }
                }
            }

            if (Input.GetAxisRaw("axePadV") == 0)
            {
                m_isAxisVInUse = false;
            }

            if (Input.GetAxisRaw("axePadH") != 0)
            {
                if (m_isAxisInUse == false)
                {
                    m_isAxisInUse = true;
                    if (Input.GetAxisRaw("axePadH") > 0.5f)
                    {
                        axeRight = true;
                       // Debug.Log("Right");
                    }
                    if (Input.GetAxisRaw("axePadH") < -0.5f)
                    {
                        axeLeft = true;
                      //  Debug.Log("Left");
                    }
                }
            }

            if (Input.GetAxisRaw("axePadH") == 0)
            {
                m_isAxisInUse = false;
            }


            

            if (Input.GetButtonDown("Up") || axeUp)
            {
                //Debug.Log("Up");
                SelectLane(1);
            }

            if (Input.GetButtonDown("Down") || axeDown)
            {
                //Debug.Log("Down");
                SelectLane(-1);
            }

            if (Input.GetButtonDown("Left") || Input.GetButtonDown("Right") || axeLeft || axeRight)
            {
                SwitchPlayer();
            }



            if (Input.GetButtonDown("UseCard"))
            {
                UseCard();
            }

        }

        if (Input.GetButtonDown("Jump"))
        {
            TakeLoot();
        }



    }

    void LeaderUpdate()
    {

        
        if(LeaderLeftTimer > 0 && LeaderLeftTimer - Time.deltaTime < 0)
        {
            LeftLeaderCardUseAnimation.Play();
        }

        if (LeaderRightTimer > 0 && LeaderRightTimer - Time.deltaTime < 0)
        {
            RightLeaderCardUseAnimation.Play();
        }

        LeaderLeftTimer -= Time.deltaTime;
        LeaderRightTimer -= Time.deltaTime;

    }

    public void PlayLeaderAction(bool isLeft)
    {
        if (isLeft)
        {
            LeaderPlayAction(true);
            LeaderLeftTimer = TimeBetweenLeaderCards * UnityEngine.Random.Range(1.0f - RandomLeaderModMultiplier, 1.0f + RandomLeaderModMultiplier);
        }
        else
        {
            LeaderPlayAction(false);
            LeaderRightTimer = TimeBetweenLeaderCards * UnityEngine.Random.Range(1.0f - RandomLeaderModMultiplier, 1.0f + RandomLeaderModMultiplier);
        }
        TimeBetweenLeaderCards -= 1;
        TimeBetweenLeaderCards = Mathf.Max(10, TimeBetweenLeaderCards);
    }

    void LeaderPlayAction(bool leftLeader)
    {

        // generate a card
        Card leadercard = CardGenerator.instance.drawLeaderCard();
        leadercard.refresh();

        // Get a random valid spawner on this side
        List<Spawner> leaderSpawners = GetRandomValidSpawnerForCard(leadercard, leftLeader);

        // Play the card
        leaderPlayCards(leadercard, leaderSpawners);

    }

    private void leaderPlayCards(Card leadercard, List<Spawner> leaderSpawners)
    {
        if (leadercard != null)
        {
            UseCardSFX.PlaySFX();
            foreach (Spawner item in leaderSpawners)
            {
                leadercard.use(item);
            }
            leadercard = null;
        }
    }



    private List<Spawner> GetRandomValidSpawnerForCard(Card leadercard, bool leftLeader)
    {
        List<Spawner> leaderSpawners = new List<Spawner>();

        int selectedLane = UnityEngine.Random.Range(0, ActiveLanes.Length + 1 - leadercard.Width);

        for (int i = 0; i < leadercard.Width; i++)
        {
            if (leadercard.laneSlots[i] == true)
            {
                if (leftLeader)
                {
                    leaderSpawners.Add(ActiveLanes[selectedLane + i].LeftSpawner);
                }
                else
                {
                    leaderSpawners.Add(ActiveLanes[selectedLane + i].RightSpawner);
                }
            }
        }

        return leaderSpawners;


    }




    private void ToggleCheatMenu()
    {

    }

    public void GameFinished(bool LeftWon)
    {
        GameOverAnimator.SetTrigger("Show");
        GameOverSFX.PlaySFX();
        HasEnded = true;
        foreach (Lane l in ActiveLanes)
        {
            if (LeftWon)
            {
                while(l.LeftSpawner.ActiveUnits.Count > 0)
                {
                    l.LeftSpawner.KillUnit(l.LeftSpawner.ActiveUnits[0]);
                }
                l.LeftSpawner.Started = false;
            }
            else
            {
                while (l.RightSpawner.ActiveUnits.Count > 0)
                {
                    l.RightSpawner.KillUnit(l.RightSpawner.ActiveUnits[0]);
                }
                l.RightSpawner.Started = false;
            }
        }
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(1);
    }

    void TakeLoot()
    {
        if (_activeLoot.Count > 0)
        {
            _activeLoot[0].LootItem();
            _activeLoot.RemoveAt(0);
        }
    }

    void UseCard()
    {
        if (currentCard != null)
        {
            UseCardSFX.PlaySFX();
            if (currentCard.affectBothSide)
            {

                foreach (Lane item in LanesSelecteds)
                {
                    currentCard.use(item);
                }
            }
            else
            {
                foreach (Spawner item in SpawnerSelected)
                {
                    currentCard.use(item);
                }
            }
            currentCard = null;
            Selecting = false;
            clearSelection();
        }
    }



    private void SwitchPlayer()
    {
        currentPlayerIsLeft = !currentPlayerIsLeft;
        //Debug.Log(currentPlayerIsLeft);
        SelectLane(0);
        LaneSelectionChangeSFX.PlaySFX();
    }


    void clearSelection()
    {
        foreach (var item in LanesSelecteds)
        {
            item.HideSelected();
        }

        foreach (var item in SpawnerSelected)
        {
            item.HideSelected();
        }

        LanesSelecteds.Clear();
        SpawnerSelected.Clear();
    }

    void HlSelection()
    {
        foreach (var item in LanesSelecteds)
        {
            item.ShowSelected();
        }

        foreach (var item in SpawnerSelected)
        {
            item.ShowSelected();
        }

    }

    void SelectLane(int dir)
    {

        LaneSelectionChangeSFX.PlaySFX();
        currentSelectedLane = currentSelectedLane + dir;

        if (currentSelectedLane < 0)
        {
            currentSelectedLane = ActiveLanes.Length - currentSelectionWidth;
        }

        if (currentSelectedLane > ActiveLanes.Length - currentSelectionWidth)
        {
            currentSelectedLane = 0;
        }
        clearSelection();
        for (int i = 0; i < currentSelectionWidth; i++)
        {
            if (currentCard.affectBothSide)
            {
                Debug.Log(currentSelectedLane + i);
                LanesSelecteds.Add(ActiveLanes[currentSelectedLane + i]);
            }
            else
            {
                if (currentCard.laneSlots[i] == true)
                {
                    if (currentPlayerIsLeft)
                    {
                        SpawnerSelected.Add(ActiveLanes[currentSelectedLane + i].LeftSpawner);

                    }
                    else
                    {
                        SpawnerSelected.Add(ActiveLanes[currentSelectedLane + i].RightSpawner);
                    }
                }
            }
        }
        HlSelection();

    }
}
