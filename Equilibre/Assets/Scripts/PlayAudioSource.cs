﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAudioSource : MonoBehaviour {

    public AudioSource SoundSource;

    private void Start()
    {
        SoundSource.volume = PlayerPrefs.GetFloat("SFX", 1f);
    }
    public void PlaySound()
    {
        SoundSource.Play();
    }
}
