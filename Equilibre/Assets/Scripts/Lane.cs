﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lane : MonoBehaviour {

    [HideInInspector]
    public int LaneIndex = -1;
    public Spawner LeftSpawner, RightSpawner;
    public Image LeftFiller, RightFiller;

    private float _spawnerDistance;

	public void InitLane(int laneIndex)
    {
        LaneIndex = laneIndex;
        LeftSpawner.InitSpawner(this, RightSpawner);
        RightSpawner.InitSpawner(this, LeftSpawner);
        _spawnerDistance = Mathf.Abs(LeftSpawner.SpawnPosition.transform.position.x - RightSpawner.SpawnPosition.transform.position.x);
    }
	
	// Update is called once per frame
	void Update () {
        UpdateFillers();
    }

    public void ShowSelected()
    {

    }

    public void HideSelected()
    {

    }

    void UpdateFillers()
    {
        float leftRefPos = LeftSpawner.ActiveUnits.Count > 0 ? LeftSpawner.ActiveUnits[0].transform.position.x : LeftSpawner.SpawnPosition.transform.position.x;
        float rightRefPos = RightSpawner.ActiveUnits.Count > 0 ? RightSpawner.ActiveUnits[0].transform.position.x : RightSpawner.SpawnPosition.transform.position.x;

        float leftFactor = (((leftRefPos + rightRefPos) * 0.5f) - LeftSpawner.SpawnPosition.transform.position.x) / _spawnerDistance;

        LeftFiller.fillAmount = leftFactor;
        RightFiller.fillAmount = 1f - leftFactor;

        if (leftFactor <= 0f)
        {
            //GAME OVER
            GameManager.instance.GameFinished(true);
        }
        else if (leftFactor >= 1f)
        {
            //GAME OVER
            GameManager.instance.GameFinished(false);
        }

    }
}
