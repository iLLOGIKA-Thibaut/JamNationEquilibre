﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubtitleControls : MonoBehaviour {

    public GameObject Subtitle;

	public void ShowSubtitle(){
        Subtitle.SetActive(true);
    }

    public void HideSubtitle()
    {
        Subtitle.SetActive(false);
    }
}
