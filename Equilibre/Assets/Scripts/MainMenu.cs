﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public Slider SFXSlider;
    public Slider MusicSlider;

    public AudioSource MusicSource;
    public AudioSource SFXSource;

	public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ChangeSFXVolume()
    {
        PlayerPrefs.SetFloat("SFX", SFXSlider.value / 10f);
        SFXSource.volume = SFXSlider.value / 10f;
        SFXSource.Play();
    }

    public void ChangeMusicVolume()
    {
        PlayerPrefs.SetFloat("Music", MusicSlider.value / 10f);
        MusicSource.volume = MusicSlider.value / 10f;
    }
}
