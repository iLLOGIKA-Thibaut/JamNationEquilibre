﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour {

    public float WalkSpeed;
    public float Life;
    public float Attack;
    public float AttackSpeed = 0.5f;
    public int AttackRange = 1;
    private float _attackTimer = 0f;
    private Vector3 _goalPosition;
    private Lane _lane;
    private Spawner _spawner;
    public int LaneIndex = -1;
    private bool _wasInRange = false;
    private float _randomValue;

    public ParticleSystem IsHitParticles;

    [Header("Animation")]
    public GameObject ObjectToAnimate;
    public float Height = 0f;
    public AnimationCurve HeightAnimation;
    private float _startHeight;
    public AnimationCurve RotationAnimation;
    public float Angle = 10f;
    public AnimationCurve SpeedAnimation;
    public float AnimationTime = 1f;
    private float _curAnimationTime = 0f;
    private float _curAnimationForce = 0f;

    private Rigidbody _rigid;

    private bool _dead = false;

    private bool _attackBoosted = false;
    private bool _defenseBoosted = false;
    private bool _speedBoosted = false;


	public void InitUnit(Lane lane, Transform spawnPos, Spawner spawner,int indexInLane)
    {
        transform.rotation = spawnPos.rotation;
        transform.position = spawnPos.transform.position;
        _goalPosition = spawnPos.transform.position;
        _lane = lane;
        _spawner = spawner;
        LaneIndex = indexInLane;
        _randomValue = Random.Range(-float.MaxValue, float.MaxValue);
        Random.seed = System.DateTime.Now.Millisecond;
        _startHeight = ObjectToAnimate.transform.localPosition.y;
        _rigid = GetComponent<Rigidbody>();
    }

    public void UpdateGoalPosition(Vector3 position)
    {
        _goalPosition = position;
    }
	
    bool InRange()
    {
        return LaneIndex <= AttackRange - 1;
    }

    public void KillEffect(Vector3 pushForce, Vector3 rotationForce)
    {
        _rigid.isKinematic = false;
        _rigid.useGravity = true;

        _rigid.AddForce(pushForce, ForceMode.VelocityChange);
    }

    public bool IsHit(float damage)
    {
        Life -= damage * (_defenseBoosted ? 0.5f : 1f);
        IsHitParticles.Play();
        if (Life <= 0)
        {
            _dead = true;
            GameManager.instance.Score++;
        }
        return Life <= 0;
    }

    public void UpdateBoosts(int attack, int defense,int speed)
    {
        _attackBoosted = attack > 0;
        _defenseBoosted = defense > 0;
        _speedBoosted = speed > 0;
    }

	// Update is called once per frame
	void Update () {
        if (_dead) return;
        if(Mathf.Abs(transform.position.x - _goalPosition.x) > 0.5f && ((_spawner.isLeft && (transform.position.x < _goalPosition.x))|| (!_spawner.isLeft && (transform.position.x > _goalPosition.x))))
        {
            transform.Translate((WalkSpeed * Time.deltaTime) * transform.forward * (_speedBoosted ? 1.3f : 1f), Space.World);
            _curAnimationForce = Mathf.Lerp(_curAnimationForce, 1f, 5f * Time.deltaTime);
            _attackTimer = 0f;
        }
        else
        {
            _curAnimationForce = Mathf.Lerp(_curAnimationForce, 0f, 15f * Time.deltaTime);
            if (InRange())
            {
                if (!_wasInRange)
                {
                    _wasInRange = true;
                    _attackTimer += ((0.5f - Random.value) * 2f) * (0.25f * AttackSpeed);
                }
                _attackTimer += Time.deltaTime;
                if (_attackTimer >= AttackSpeed && !GameManager.instance.HasEnded)
                {
                    _spawner.SendAttack(Attack, 0, 1);
                    _attackTimer = 0f;
                }
            }
            else {
                _attackTimer = 0f;
                _wasInRange = false;
            }
            
        }
        UpdateMovementAnimation();
	}

    void UpdateMovementAnimation()
    {
        ObjectToAnimate.transform.localPosition = new Vector3(0f, (HeightAnimation.Evaluate(_curAnimationTime / AnimationTime) * _curAnimationForce * Height) + _startHeight, 0f);
        ObjectToAnimate.transform.localRotation = Quaternion.Euler(RotationAnimation.Evaluate(_curAnimationTime / AnimationTime) * _curAnimationForce * Angle, 0f, 0f);
        _curAnimationTime += Time.deltaTime;
        if (_curAnimationTime > AnimationTime) _curAnimationTime -= AnimationTime;
    }
}
